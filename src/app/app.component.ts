import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { ContactusPage } from '../pages/contactus/contactus';
import { SupportPage } from '../pages/support/support';
import { FaqPage } from '../pages/faq/faq';
import { OrderPage } from '../pages/order/order';
import { SignupPage } from '../pages/signup/signup';
import { VideosPage } from '../pages/videos/videos';
import { UsagePage } from '../pages/usage/usage';
import { ProfilePage } from '../pages/profile/profile';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SignupPage;
  deviceType: any;

  pages: Array<{icon: any, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private push: Push, public alertCtrl: AlertController, public events: Events) {
    this.initializeApp();

    events.subscribe('user:login', () => {
      this.initPushNotification();
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'assets/img/home.png', title: 'Home', component: HomePage },
      { icon: 'assets/img/usage-benefits.png', title: 'Usage & Benefits', component: UsagePage },
      { icon: 'assets/img/youtube.png', title: 'Videos', component: VideosPage },
      { icon: 'assets/img/order-icon.png', title: 'Order an Accessory', component: OrderPage },
      { icon: 'assets/img/support.png', title: 'Support', component: SupportPage },
      { icon: 'assets/img/faq.png', title: 'FAQs', component: FaqPage },
      { icon: 'assets/img/phone-book.png', title: 'Contact Us', component: ContactusPage },
      { icon: 'assets/img/man-user.png', title: 'User Profile', component: ProfilePage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if(localStorage.getItem('aqua-token')){
        this.rootPage=HomePage;
      }else{
        this.rootPage=SignupPage;
      }
      this.initPushNotification();
      //this.rootPage=HomePage;
    });
  }

  initPushNotification(){
    if(this.platform.is('ios')){
      this.deviceType = 'ios';
    } else if(this.platform.is('android')){
      this.deviceType = 'android';
    }
    // to check if we have permission
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We don\'t have permission to send push notifications');
        }
      });

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: '38903591715',
        icon: 'icon',
        iconColor: "#000000",
        forceShow: true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    pushObject.on('notification').subscribe((notification: any) =>{
      console.log('Received a notification', notification);
      const type = notification.additionalData.type;
      if(type == 'faq'){
        this.nav.push(FaqPage, notification);
      } else if(type == 'videos'){
        this.nav.push(VideosPage, notification);
      } else if(type == 'usage'){
        this.nav.push(UsagePage, notification);
      }
    });
    pushObject.on('registration').subscribe((registration: any) => {
      console.log('Device registered', registration);
      const device = {
        token : registration['registrationId'],
        type: this.deviceType
      }
      localStorage.setItem('device',JSON.stringify(device));
      console.log('device',device);
    });
    
    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  logout(){
    this.nav.setRoot(LoginPage);
    localStorage.removeItem('aqua-token');
  }
}
