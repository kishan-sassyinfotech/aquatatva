import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController, Events, LoadingController, AlertController } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {HomePage} from '../home/home';
import { Http } from '@angular/http';
import {AllPostProvider} from '../../providers/all-post/all-post';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import { NgForm } from '@angular/forms';


import { InAppBrowser } from '@ionic-native/in-app-browser';
import{ Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
// import firebase from 'firebase';

/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  providers : [AllPostProvider]
})
export class SignupPage {
  message = false;
  device : any;
  messageFalse = false;
  signupForm = {};
  signUpData = {
    fname : "",
    lname: "",
    email: "",
    password: "",
    userId:"",
    deviceToken: "",
    deviceType: ""
  }
  googleSignUpData = {
    fname : "",
    lname: "",
    email: "",
    userId:"",
    deviceToken: "",
    deviceType: ""
  }

  facebookSignUpData = {
    fname : "",
    lname: "",
    email: "",
    userId:"",
    deviceToken: "",
    deviceType: ""
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,public FormBuilder: FormBuilder,public menu:MenuController,public http:Http, public signupProvider: AllPostProvider, private iab:InAppBrowser,public fb:Facebook, private googlePlus: GooglePlus, public events: Events, public loadingCtrl: LoadingController,private alertCtrl: AlertController) {
    this.device = JSON.parse(localStorage.getItem('device'));
  }

  facebookAuth(){
    let device = JSON.parse(localStorage.getItem('device'));
    // const facebooklogin=this.iab.create('https://aquatatva.herokuapp.com/api/auth/facebook','_self','location=no');
    this.fb.login(['public_profile', 'user_friends', 'email']).then((res: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name,last_name', []).then(profile => {
        console.log('profile',profile);
        this.facebookSignUpData.fname = profile['first_name'];
        this.facebookSignUpData.lname = profile['last_name'];
        this.facebookSignUpData.email = profile['email'];
        this.facebookSignUpData.userId = profile['id'];
        this.facebookSignUpData.deviceToken = device ? device['token'] : null;
        this.facebookSignUpData.deviceType =  device ? device['type'] : null;
        console.log('this.facebookSignUpData',this.facebookSignUpData);
        let loading = this.loadingCtrl.create({
          content: 'Please wait...',
          dismissOnPageChange: true
        });
        loading.present();
        this.signupProvider.loginWithFb(this.facebookSignUpData).subscribe(data=>{
          loading.dismiss();
          if(data){
            localStorage.setItem('aqua-token',data.token);
            this.navCtrl.setRoot(HomePage);
          } else {
            console.log('no data');
          }
        }, err => {
          loading.dismiss();
          this.presentAlert(err._body);
          console.log(err);
        });
      }, err => {
        this.presentAlert(err._body);
        console.log(err);
      });
      console.log('Logged into Facebook!', res);
    })
    
    .catch(e => { 
      console.log('Error logging into Facebook', e) 
    });
  }

  googleAuth(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    let device = JSON.parse(localStorage.getItem('device'));
    loading.present();
    this.googlePlus.login({})
    .then(res => {
      this.googleSignUpData.fname = res.givenName;
      this.googleSignUpData.lname = res.familyName;
      this.googleSignUpData.email = res.email;
      this.googleSignUpData.userId = res.userId;
      this.googleSignUpData.deviceToken = device ? device['token'] : null;
      this.googleSignUpData.deviceType = device ? device['type'] : null;

      this.signupProvider.loginWithGoogle(this.googleSignUpData).subscribe(data=>{
        loading.dismiss();
        if(data){
          localStorage.setItem('aqua-token',data.token);
          this.navCtrl.setRoot(HomePage);
        } else {
          console.log('no data');
        }
      }, err => {
        loading.dismiss();
        this.presentAlert(err._body);
        console.log(err);
      });
    })
    .catch(err => {
      console.error(err);
      loading.dismiss();
    });
  }

   signup(signupValue){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    let device = JSON.parse(localStorage.getItem('device'));
    console.log('device1',device);
    loading.present();
    this.signUpData.fname = signupValue.firstname;
    this.signUpData.lname = signupValue.lastname;
    this.signUpData.email = signupValue.email;
    this.signUpData.password = signupValue.password;
    this.signUpData.deviceToken = device ? device['token'] : null;
    this.signUpData.deviceType = device ? device['type'] : null;
    console.log(this.signUpData);
    this.signupProvider.saveSignup(this.signUpData).subscribe(data=>{
      loading.dismiss();
      // successfully created user
      if(data){
        localStorage.setItem('aqua-token',data.token);
        // this.message = true;
        // setTimeout(function(){
        //   this.message = false;
        // },6000);
        this.navCtrl.setRoot(HomePage);        
      }else {
        // this.messageFalse = true;
        // setTimeout(function(){
        //   this.messageFalse = false;
        // },6000);
      }
    }, err => {
      loading.dismiss();
      this.presentAlert(err._body);
      console.log(err);
    });
  }
  // signup google 
  // signupGoogle(){
  //   this.signupProvider.google().subscribe(data=>{
  //     console.log(data,'what is data');
  //   });
  // }
  // signup google end
  loginRedirect(){
  this.navCtrl.push(LoginPage);
  }
  homeRoute(){
  this.navCtrl.push(HomePage);
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
    this.menu.enable(false);
  }

  presentAlert(body) {
    body = JSON.parse(body);
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: body.error,
      buttons: ['Ok']
    });
    alert.present();
  }
}
