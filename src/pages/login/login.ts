import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController, LoadingController, AlertController } from 'ionic-angular';
import {SignupPage} from '../signup/signup';
import {HomePage} from '../home/home';
import { Http } from '@angular/http';
import {AllPostProvider} from '../../providers/all-post/all-post';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import { NgForm } from '@angular/forms';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers : [AllPostProvider]
})
export class LoginPage { 
  loginForm = {};
  device : any;
  googleSignUpData = {
    fname : "",
    lname: "",
    email: "",
    userId:"",
    deviceToken: "",
    deviceType: ""
  }
  facebookSignUpData = {
    fname : "",
    lname: "",
    email: "",
    userId:"",
    deviceToken: "",
    deviceType: ""
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu:MenuController,public FormBuilder: FormBuilder, public http : Http, public loginProvider: AllPostProvider, private googlePlus: GooglePlus,public loadingCtrl: LoadingController, private alertCtrl: AlertController, public fb:Facebook) {
    this.device = JSON.parse(localStorage.getItem('device'));
  }
  signupRedirect(){
     this.navCtrl.push(SignupPage);
  }
  homeRoute(){
    this.navCtrl.push(HomePage);
  }
  login(loginForm){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    let device = JSON.parse(localStorage.getItem('device'));
    //const url = 'https://aquatatva.herokuapp.com/api/login';
    const url = 'http://admin.aquatatva.com/index.php/api/login';
    
    this.http.post(url,{
      email: loginForm.email,
      password: loginForm.password,
      deviceToken: device ? device['token'] : null,
      deviceType: device ? device['type'] : null
    }).map(res => res.json()).subscribe(data =>{
      loading.dismiss();
      if(data){
        this.navCtrl.setRoot(HomePage); 
        localStorage.setItem('aqua-token', data.token);
        console.log(localStorage.getItem('aqua-token'),'token');
      }else {

      }

    }, err => {
      loading.dismiss();
      this.presentAlert(err._body);
      console.log(err);
    });
  }

  facebookAuth(){
    let device = JSON.parse(localStorage.getItem('device'));
    // const facebooklogin=this.iab.create('https://aquatatva.herokuapp.com/api/auth/facebook','_self','location=no');
    this.fb.login(['public_profile', 'user_friends', 'email']).then((res: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name,last_name', []).then(profile => {
        console.log('profile',profile);
        this.facebookSignUpData.fname = profile['first_name'];
        this.facebookSignUpData.lname = profile['last_name'];
        this.facebookSignUpData.email = profile['email'];
        this.facebookSignUpData.userId = profile['id'];
        this.facebookSignUpData.deviceToken = device ? device['token'] : null;
        this.facebookSignUpData.deviceType =  device ? device['type'] : null;
        console.log('this.facebookSignUpData',this.facebookSignUpData);
        let loading = this.loadingCtrl.create({
          content: 'Please wait...',
          dismissOnPageChange: true
        });
        loading.present();
        this.loginProvider.loginWithFb(this.facebookSignUpData).subscribe(data=>{
          loading.dismiss();
          if(data){
            localStorage.setItem('aqua-token',data.token);
            this.navCtrl.setRoot(HomePage);
          } else {
            console.log('no data');
          }
        }, err => {
          loading.dismiss();
          this.presentAlert(err._body);
          console.log(err);
        });
      }, err => {
        this.presentAlert(err._body);
        console.log(err);
      });
      console.log('Logged into Facebook!', res);
    })
    
    .catch(e => { 
      console.log('Error logging into Facebook', e) 
    });
  }

  googleAuth(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    let device = JSON.parse(localStorage.getItem('device'));
  
    loading.present();
    this.googlePlus.login({})
    .then(res => {
      this.googleSignUpData.fname = res.givenName;
      this.googleSignUpData.lname = res.familyName;
      this.googleSignUpData.email = res.email;
      this.googleSignUpData.userId = res.userId;
      this.googleSignUpData.deviceToken =  device ? device['token'] : null;
      this.googleSignUpData.deviceType = device ? device['type'] : null;

      this.loginProvider.loginWithGoogle(this.googleSignUpData).subscribe(data=>{
        loading.dismiss();
        if(data){
          localStorage.setItem('aqua-token',data.token);
          this.navCtrl.setRoot(HomePage);
        } else {
          console.log('no data');
        }
      }, err => {
        loading.dismiss();
        this.presentAlert(err._body);
        console.log(err);
      });
    })
    .catch(err => { 
      loading.dismiss();
      console.error(err);
    });
  }
  // login(loginForm){
  //   this.loginProvider.loginView(loginForm).subscribe(data=>{
  //     if(data){
  //       this.navCtrl.setRoot(HomePage); 
  //       localStorage.setItem('token', data.token);
  //       console.log(localStorage.getItem('token'),'token');
  //     }else {

  //     }
  //   })
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.enable(false);
  }
  loginSwipe($event) {
  	console.log($event,'event')
	}

  presentAlert(body) {
    body = JSON.parse(body);
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: body.error,
      buttons: ['Ok']
    });
    alert.present();
  }
}
