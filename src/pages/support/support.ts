import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { AllPostProvider } from '../../providers/all-post/all-post';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import { NgForm } from '@angular/forms';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions  } from '@ionic-native/camera';

/**
 * Generated class for the SupportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
  providers : [AllPostProvider]
})
export class SupportPage {
  message = false;
  supportDone = "";
  supportForm = {};
  imageURI:any;
imageFileName:any;

fileTransfer: FileTransferObject = this.transfer.create();
  
  // let fileSelect =  document.querySelector('input[type=file]').files[0];
  // console.log(fileSelect,'file');
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public FormBuilder: FormBuilder, 
    public http:Http, 
    private transfer: FileTransfer,
    private camera: Camera,
    private alertCtrl: AlertController,
    public supportProvider: AllPostProvider) {
   
  }
    // formSupport(supportForm) {
    //   console.log(this.supportForm,'supportForm',supportForm,supportForm.form._value); 
    //   let valueForm = supportForm.form._value;
    //   console.log(this.valueForm,'dfgh');
    //   supportForm = {
    //     name: valueForm.name,
    //     contact: valueForm.contact,
    //     email: valueForm.email,
    //     address: valueForm.address,
    //     suggestion: valueForm.suggestion,
    //     supporttype: valueForm.supporttype,
    //     image: valueForm.Image
    //   } 
    //   console.log(this.supportForm,'valueForm');
    //   this.supportProvider.saveSupportData(this.supportForm).subscribe(data=>{
    //     this.supportDone = data;
    //     if(data.message == "successfully Registered Support Request"){
    //       this.message = true; 
    //       setTimeout(function(){
    //         this.message = false;
    //         console.log('out');
    //       },3000);
    //     }else {
    //       this.message = false;
    //     }
    //   console.log(this.supportForm,'supportForm');
    //   });
    // }

    formSupport(supportForm) {
      //if(this.imageURI){
        //this.upload(this.imageURI);
      //} else {
        console.log('supportForm',supportForm);
        this.supportProvider.saveSupportData(supportForm).subscribe(data=>{
          console.log('data',data);
          if(this.imageURI){
            this.upload(this.imageURI, data['data']['id'],data['message']);
          } else {
            this.presentAlert(data['message'],true);
          }
          //this.presentAlert('Successfully Registered Support Request',true);
          // this.supportDone = data;
          // if(data.message == "successfully Registered Support Request"){
          //   this.message = true;
          //   setTimeout(function(){
          //     this.message = false;
          //   }.bind(this),3000);
          // }else {
          //   this.message = false;
          // }
        }, (err) => {
          this.presentAlert(err._body, false);
          console.log('err opening photo library',err);
         });
      //}
    }

    selectPicture(){
      const galleryOptions: CameraOptions = {
        quality: 50,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.FILE_URI,
        mediaType: this.camera.MediaType.PICTURE,
        encodingType:this.camera.EncodingType.JPEG,
      }
  
      this.camera.getPicture(galleryOptions).then((imageData) => {
        this.imageURI = imageData;
        // this.upload(imageData);
       }, (err) => {
        console.log('err opening photo library',err);
       });
    }

    upload(imageData, id, message){
      console.log('id',id);
      let options: FileUploadOptions = {
        fileKey: 'image',
        fileName: 'name.jpg',
        chunkedMode : false,
     }
     let url = this.supportProvider['url'] + 'storesupportimage/' + id;
      this.fileTransfer.upload(imageData, url, options)
        .then((data) => {
        console.log(data," Uploaded Successfully");
        this.presentAlert(message,true);
      }, (err) => {
        console.log(err);
      });
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }

  presentAlert(msg, isSuccess) {
    let alert = this.alertCtrl.create({
      title: isSuccess ? 'Info' : 'Error',
      subTitle: msg,
      buttons: ['Ok']
    });
    alert.present();
  }

}
