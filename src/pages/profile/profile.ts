import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import {AllPostProvider} from '../../providers/all-post/all-post';
import {FormGroup, FormBuilder, FormControl, Validators} from "@angular/forms";
import { NgForm } from '@angular/forms';

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
 	editView = true;
	userForm = {
		fname : "",
		lname: "",
		email: "",
		phone: "",
		notification: "",
		provider: "",
		city: "",
		state: "",
		country: "",
		token:"",
		id: "",
	};
 	userData = {};
  constructor(
	  public navCtrl: NavController, 
	  public navParams: NavParams,
	  public FormBuilder: FormBuilder, 
	  public http : Http, 
	  private alertCtrl: AlertController,
	  public loginProvider: AllPostProvider) {
  }

  ionViewDidLoad() {
  	let token = localStorage.getItem('aqua-token');
	console.log('ionViewDidLoad ProfilePage');
	const url = 'http://admin.aquatatva.com/index.php/api/show/'+token+'';
	//const url = 'https://aquatatva.herokuapp.com/api/show/'+token+'';
      	this.http.get(url).map(res => res.json()).subscribe(data =>{
      if(data){
        this.userData = data[0];
        // console.log(this.userData,'userData');
        // this.userForm.first_name = this.userData.first_name; 
        // this.userForm.last_name = this.userData.last_name; 
        // this.userForm.mobile = this.userData.mobile; 
        // this.userForm.email = this.userData.email; 
        // this.userForm = this.userData;
        console.log(this.userForm,'userForm');
      }else {

      }
    })
  }

  editProfile(){
  	console.log('edit');
  	this.editView = false;
  }
  userProfile(formData){
	console.log(formData,'formdata');
	  //const url = 'https://aquatatva.herokuapp.com/api/updateuser/'+formData.id+'';
	  const url = 'http://admin.aquatatva.com/index.php/api/updateuser/'+formData.id+'';
  	this.http.put(url,{
  		fname: formData.fname,
  		lname: formData.lname,
  		id: formData.id,
  		email: formData.email,
  		phone: formData.phone,
  		notification: formData.notification,
  		provider: formData.provider,
  		city: formData.city,
  		state: formData.state,
  		country: formData.country,
  		token: formData.token,
  		created_at: formData.created_at,
  		updated_at: formData.updated_at,
  		address: formData.address,
  	}).map(res => res.json()).subscribe(data =>{
  		if(data){
			let alert = this.alertCtrl.create({
				title: 'Success',
				subTitle: 'Your profile is updated successfully',
				buttons: ['Ok']
			});
			alert.present();
  		}
  	})
  }
}

